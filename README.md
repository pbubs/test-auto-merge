## Usage

First install the dependencies:

```sh
npm install
```

Then you can:

| Command               | Description                                  |
| --------------------- | -------------------------------------------- |
| **`npm run start`**   | Run your website on http://localhost:8080    |
| **`npm run build`**   | Build your production website inside `/dist` |
| **`npm run format`**  | Run prettier on all filles except `/dist`    |
| **`npm run analyze`** | Output info on your bundle size              |

That's it.

## eli5 (explain like i'm 5)

Webpack is used when:

1. Any changes to `assets/scripts` or `assets/styles` is watched and rebuilt by webpack.
1. The new files are appended to the ignored file `_includes/webpack.njk` thanks to [html-webpack-plugin](https://github.com/jantimon/html-webpack-plugin).
1. Eleventy sees the new `_includes/webpack.njk` and rebuild.

Any other changes is picked up normally by Eleventy (see [.eleventy.js](.eleventy.js))

## Deploying and previewing on Squiz

- https://www.monash.edu/policy-bank/_nocache?SQ_DESIGN_NAME=mango-new
- Design custimisation #2292180
- Gitbridge #2288921
- Nested JS/CSS #2292186
- Webhook has been setup to auto pull from master
- Nested components #2314911
- Preview folder #2314826

## Included

- Barebone eleventy (literally :scream:)
- Fast build with per env configs ([babel-env](https://babeljs.io/docs/en/babel-preset-env), [postcss-preset-env](https://github.com/csstools/postcss-preset-env), [webpack](https://webpack.js.org/configuration/#use-different-configuration-file)...)
- `.js` (ES6, Babel and its polyfills)
- `.css` (Sass, Autoprefixer)
- [Prettier](https://prettier.io/) for formatting

### Pull request reviews

[] Provide a URL to preview
[] Ensure it has met acceptance criteria
[] Tested on mobile/desktop viewports
[] BEM naming standards
[] Use of variable for fonts and colour
