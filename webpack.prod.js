const dev = require('./webpack.dev.js');
const merge = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = merge(dev, {
  mode: 'production',
  watch: false,
  devtool: 'source-map',
  output: { filename: 'rebuild.js' },
  optimization: {
    minimizer: [
      new TerserPlugin({
        sourceMap: false
      }),
      new OptimizeCssAssetsPlugin({
        cssProcessor: require('cssnano'),
        cssProcessorOptions: { map: false }, ecssProcessorPluginOptions: {
          preset: ['default', { discardComments: { removeAll: true } }],
        },
        canPrint: true
      })
    ]
  }
});
