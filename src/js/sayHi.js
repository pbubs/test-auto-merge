function sayHi(user) {
    console.log(`G'day, ${user}!`);
}

function sayBye(user) {
    console.log(`Bye, ${user}!`);
}

export { sayHi, sayBye };