import { policyPageJS } from '../_includes/partials/modules/policy-page/js/policy-page.js';
policyPageJS();

import { initBannerSearch } from '../_includes/partials/modules/banner-search/js/global';

import { initMicrobannerSearch } from '../_includes/partials/modules/microbanner/js/global';

import { initSearchPage } from '../_includes/partials/modules/search-listing/js/global';

import { stickyNav } from '../_includes/partials/modules/sticky-nav/js/sticky-nav.js';
//stickyNav();

function runOnWindowsLoad(callback) {
  if (document.readyState === 'complete') {
    callback();
  } else {
    window.addEventListener('load', callback);
  }
}

runOnWindowsLoad(stickyNav);

window.addEventListener('DOMContentLoaded', () => {
  if (
    document.querySelectorAll('.banner.banner-search .banner-search__icon')
      .length > 0
  ) {
    console.log('initialising Banner Search');
    initBannerSearch();
  } else {
    console.log('Banner Search not detected, not intialising search');
  }

  if (document.querySelectorAll('#microbanner-search-form').length > 0) {
    console.log('initialising Micro Banner Search');
    initMicrobannerSearch();
  } else {
    console.log('Micro Banner Search not detected, not intialising search');
  }

  if (document.querySelectorAll('.cards-grid.search-listing').length > 0) {
    console.log('initialising Search Page');
    initSearchPage();
  } else {
    console.log('Search listing not found, not initialising');
  }
});
