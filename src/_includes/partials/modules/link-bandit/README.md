# Link Bandit

Hijacks external links and applies the following behaviour:

* Sets a blank browsing context to open the link in a new tab/window
* Applies an "opens in external window" icon style
* Applies an external [link relation](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/link#attr-rel) (see: [link type values](https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types))
