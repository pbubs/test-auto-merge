'use strict';
/*eslint no-multi-spaces: 0*/

var fakeLodash,
    fakeURL,
    LinkBandit;

fakeLodash  = fakeLodash || {};
fakeURL     = fakeURL || {};

/*
Array.prototype.find polyfill
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find
*/
/* eslint-disable no-bitwise, no-extend-native */
if (!Array.prototype.find) {
    Array.prototype.find = function(predicate) {
        if (this === null) {
            throw new TypeError('Array.prototype.find called on null or undefined');
        }
        if (typeof predicate !== 'function') {
            throw new TypeError('predicate must be a function');
        }
        var list = Object(this);
        var length = list.length >>> 0;
        var thisArg = arguments[1];
        var value;

        for (var i = 0; i < length; i+=1) {
            value = list[i];
            if (predicate.call(thisArg, value, i, list)) {
                return value;
            }
        }
        return undefined;
    };
}

/* Wrapper to use Node URL module */
function parseURL(fullURL, field) {
    var tempLink;
    tempLink        = document.createElement('a');
    tempLink.href   = fullURL;
    return tempLink[field];
}

// Borrow variables and methods from Lodash v4.11.1
fakeLodash.vars = {
    objToString: Object.prototype.toString,
    boolTag:     '[object Boolean]',
    stringTag:   '[object String]'
};

fakeLodash.isBoolean = function(value) {
    return value === true || value === false || (fakeLodash.isObjectLike(value) && fakeLodash.vars.objToString.call(value) === fakeLodash.vars.boolTag); // eslint-disable-line max-len
};

fakeLodash.isObjectLike = function(value) {
    return !!value && typeof value === 'object';
};

fakeLodash.isString = function(value) {
    return typeof value === 'string'
        || fakeLodash.isObjectLike(value)
        && fakeLodash.vars.objToString.call(value) === fakeLodash.vars.stringTag;
};

// Browser Location interface does not work in Node, so an alternative parser can be passed in.
fakeURL.parse = function(fullURL, field, altParseURL) {
    if (typeof altParseURL === 'undefined' || !altParseURL) {
        return parseURL(fullURL, field);
    }
    // Node URL module uses different syntax
    return altParseURL(fullURL)[field];
};

LinkBandit = {
    defaults: {
        iconClass: 'link--external'
    },
    /*
     * Applies external link relation to element.
     */
    applyExternalRel: function(selector, $) {
        $(selector).each(function(i, el) {
            var linkRel = $(el).attr('rel'),
                linkRelValues = [],
                externalRelExists = false;
            // Link "rel" attribute doesn't exist yet - define a new relation
            if (typeof linkRel === 'undefined') {
                $(el).attr('rel', 'external');
            }
            // Link already has "rel" attribute set - non-destructively insert "external" relation
            else {
                linkRelValues = linkRel.replace(/[\s][\s]+/g, ' ').trim().split(' ');
                // Check if external relation already exists
                externalRelExists = linkRelValues.find(function(currentRelValue) {
                    return currentRelValue === 'external';
                });
                // Append external relation to list of relations
                if (!externalRelExists) {
                    linkRelValues.push('external');
                    externalRelExists = true;
                }
                // Change existing "rel" attribute
                $(el).attr('rel', linkRelValues.join(' '));
            }
        });
    },
    /*
     * Applies external link icon style to element.
     */
    applyIcon: function(selector, iconClass, $) {
        var args = Array.prototype.slice.call(arguments),
            countArgs = args.length;
        // Overload: applyIcon(selector, $)
        if (countArgs === 2) {
            $ = args[1];
            iconClass = LinkBandit.defaults.iconClass;
        }
        $(selector).addClass(iconClass);
    },
    /*
     * Base version of isExternalLink() function. All arguments are mandatory.
     */
    baseIsExternalLink: function(checkLink, internalLocations, altParseURL) {
        var isExternal = true,
            linkHost;
        linkHost = fakeURL.parse(checkLink, 'hostname', altParseURL);
        // Undefined or empty/blank host is always treated as an internal link
        if (typeof linkHost === 'undefined' || !linkHost || !linkHost.trim().length) {
            return false;
        }
        // Push single internal location to array for consistency
        if (fakeLodash.isString(internalLocations)) {
            internalLocations = [internalLocations];
        }
        // Check against all multiple internal locations
        isExternal = !internalLocations.find(function(currentInternalHost) {
            var currentHostRegExp = new RegExp('(^|\\.)' + currentInternalHost + '$', 'i');
            return currentHostRegExp.test(linkHost);
        });
        // Link is external
        return isExternal;
    },
    /*
     * Filters a list of elements and returns elements containing an external link.
     */
    findExternalLinks: function(selector, internalLocations, altParseURL, $) {
        var args = Array.prototype.slice.call(arguments),
            countArgs = args.length,
            externalLinks = [];
        // Overload: findExternalLinks(selector, $)
        if (countArgs === 2) {
            $ = args[1];
            internalLocations = window.location.hostname;
            altParseURL = parseURL;
        }
        else if (countArgs === 3) {
            // Overload: findExternalLinks(selector, altParseURL, $)
            if (fakeLodash.isBoolean(args[1])) {
                altParseURL = args[1];
                $ = args[2];
                internalLocations = window.location.hostname;
            }
            // Overload: findExternalLinks(selector, internalLocations, $)
            else {
                internalLocations = args[1];
                $ = args[2];
                altParseURL = parseURL;
            }
        }
        externalLinks = $(selector).filter(function(i, el) {
            var linkURL = $(el).attr('href');
            return LinkBandit.isExternalLink(linkURL, internalLocations, altParseURL);
        });
        return externalLinks;
    },
    /*
     * Hijacks external links:
     *
     * - Applies blank browser context
     * - Adds external link relation
     * - Applies external link icon
     */
    init: function(selector, internalLocations, iconClass, altParseURL, $) {
        var args = Array.prototype.slice.call(arguments),
            countArgs = args.length,
            externalLinks;
        // Overload: init(selector, internalLocations, iconClass, $)
        if (countArgs === 4) {
            altParseURL = null;
            $ = args[3];
        }
        externalLinks = LinkBandit.findExternalLinks(selector, internalLocations, altParseURL, $);
        externalLinks.each(function(i, el) {
            // Set blank browser context (opens link in new tab/window)
            $(el).attr('target', '_blank');
            // Apply external link relation
            LinkBandit.applyExternalRel(el, $);
            // Apply external link icon
            LinkBandit.applyIcon(el, iconClass, $);
        });
    },
    /*
     * Checks if a given URL is an external link by comparing it to a list of known internal hosts.
     */
    isExternalLink: function(checkLink, internalLocations, altParseURL) {
        var args = Array.prototype.slice.call(arguments),
            countArgs = args.length;
        // Overload: isExternalLink(checkLink)
        if (countArgs === 1) {
            internalLocations = window.location.hostname;
            altParseURL = parseURL;
        }
        else if (countArgs === 2) {
            // Overload: isExternalLink(checkLink, internalLocations)
            if (fakeLodash.isBoolean(args[1])) {
                altParseURL = parseURL;
            }
            // Overload: isExternalLink(checkLink, altParseURL)
            else {
                internalLocations = window.location.hostname;
            }
        }
        return LinkBandit.baseIsExternalLink(checkLink, internalLocations, altParseURL);
    }
};

// Safe export
if ((typeof module !== 'undefined') && (typeof module.exports !== 'undefined')) {
    module.exports = LinkBandit;
}
