'use strict';
/* eslint-env mocha */
/*eslint no-multi-spaces: 0*/

/**
 * Tests for Link Bandit.
 *
 * Run by using:
 * mocha --reporter=nyan source/modules/link-bandit/tests/spec.js
 */
var expect      = require('chai').expect,
    LinkBandit  = require('../js/plugin.js'),
    cheerio     = require('cheerio'),
    url         = require('url');

describe('LinkBandit', function() {
    it('should exist', function() {
        expect(LinkBandit).to.not.be.undefined;
    });
});

describe('applyExternalRel()', function() {
    it('should apply an external relation to targeted elements', function() {
        var source = '<html><body>'
                + '<a href="http://fake.url/1" class="target">Link with external rel 1</a>'
                + '<a href="http://fake.url/2" class="target">Link with external rel 2</a>'
                + '</body></html>',
            $ = cheerio.load(source);
        LinkBandit.applyExternalRel('.target', $);
        $('.target').each(function(i, el) {
            expect($(el).attr('rel')).to.equal('external');
        });
    });
    it('should non-destructively append the external relation to a list of existing relations', function() {
        var source = '<html><body>'
                + '<a href="http://fake.url/1" class="target">Link with external rel 1</a>'
                + '<a href="http://fake.url/2" class="target" rel="noindex">Link with external rel 2</a>'
                + '</body></html>',
            $ = cheerio.load(source);
        LinkBandit.applyExternalRel('.target', $);
        $('.target').each(function(i, el) {
            if (i === 0) {
                expect($(el).attr('rel')).to.equal('external');
            }
            else {
                expect($(el).attr('rel')).to.equal('noindex external');
            }
        });
    });
    it('should not add a duplicate relation if an external relation already exists', function() {
        var source = '<html><body>'
                + '<a href="http://fake.url/1" class="target">Link with external rel 1</a>'
                + '<a href="http://fake.url/2" class="target" rel="external">Link with external rel 2</a>'
                + '<a href="http://fake.url/3" class="target" rel="noindex external prev">Link with external rel 3</a>'
                + '</body></html>',
            $ = cheerio.load(source);
        LinkBandit.applyExternalRel('.target', $);
        $('.target').each(function(i, el) {
            if (i === 2) {
                expect($(el).attr('rel')).to.equal('noindex external prev');
            }
            else {
                expect($(el).attr('rel')).to.equal('external');
            }
        });
    });
});

describe('applyIcon()', function() {
    it('should apply a specified icon class to targeted elements', function() {
        var source = '<html><body>'
                + '<a href="http://fake.url/1" class="target">Link with icon 1</a>'
                + '<a href="http://fake.url/2" class="target">Link with icon 2</a>'
                + '</body></html>',
            $ = cheerio.load(source);
        LinkBandit.applyIcon('.target', 'link__custom-external', $);
        $('.target').each(function(i, el) {
            expect($(el).hasClass('link__custom-external')).to.equal(true);
        });
    });
    it('should fall back to a default icon class if none is specified', function() {
        var source = '<html><body>'
                + '<a href="http://fake.url/1" class="target">Link with icon 1</a>'
                + '<a href="http://fake.url/2" class="target">Link with icon 2</a>'
                + '</body></html>',
            $ = cheerio.load(source),
            defaultClass = 'link--external';
        LinkBandit.applyIcon('.target', $);
        $('.target').each(function(i, el) {
            expect($(el).hasClass(defaultClass)).to.equal(true);
        });
    });
});

describe('findExternalLinks()', function() {
    it('should parse a list of elements and return a filtered list of elements that do not match the current host', function() { // eslint-disable-line max-len
        var source = '<html><body><a href="http://internal.url/1" class="target">Link 1 (internal)</a>'
                + '<a href="http://external.url/2" class="target">Link 2 (external)</a>'
                + '<a href="http://external.url/3" class="target">Link 3 (external)</a>'
                + '<a href="http://another.url/4" class="target">Link 4 (external)</a></body></html>',
            $ = cheerio.load(source),
            internalLocation = 'internal.url',
            expectedResult = [],
            actualResult = LinkBandit.findExternalLinks('.target', internalLocation, url.parse, $);
        expectedResult = $('.target').filter(function(i) { // eslint-disable-line consistent-return
            if (i === 1 || i === 2 || i === 3) {
                return true;
            }
        });
        expect(actualResult).to.eql(expectedResult);
    });
    it('should allow multiple internal locations to be specified', function() {
        var source = '<html><body><a href="http://internal.url/1" class="target">Link 1 (internal)</a>'
                + '<a href="http://external.url/2" class="target">Link 2 (external)</a>'
                + '<a href="http://external.url/3" class="target">Link 3 (external)</a>'
                + '<a href="http://another.url/4" class="target">Link 4 (external)</a>'
                + '<a href="http://another.url/5">Link 5 (external, not targeted)</a>'
                + '<a href="http://another-internal.url/6" class="target">Link 6 (internal)</a>'
                + '<a href="http://also.this.one/7" class="target">Link 7 (internal)</a>'
                + '<a href="http://not-internal.url/8" class="target">Link 8 (external)</a></body></html>',
            $ = cheerio.load(source),
            internalLocations = ['internal.url', 'another-internal.url', 'also.this.one'],
            expectedResult = [],
            actualResult = LinkBandit.findExternalLinks('.target', internalLocations, url.parse, $);
        expectedResult = $('.target').filter(function(i) { // eslint-disable-line consistent-return
            if (i === 1 || i === 2 || i === 3 || i === 6) {
                return true;
            }
        });
        expect(actualResult).to.eql(expectedResult);
    });
    // should ignore anchors for the current page
    // should treat undefined or blank host as an internal location
});

describe('init', function() {
    it('should find external links and set a blank browsing context', function() {
        var source = '<html><body><a href="http://internal.url/1" class="target">Link 1 (internal)</a>'
                + '<a href="http://external.url/2" class="target">Link 2 (external)</a>'
                + '<a href="http://external.url/3" class="target">Link 3 (external)</a>'
                + '<a href="http://another.url/4" class="target">Link 4 (external)</a></body></html>',
            $ = cheerio.load(source),
            internalLocation = 'internal.url';
        LinkBandit.init('.target', internalLocation, 'link__custom-external', url.parse, $);
        $('.target').each(function(i, el) {
            if (i === 1 || i === 2 || i === 3) {
                expect($(el).attr('target')).to.equal('_blank');
            }
            else {
                expect($(el).attr('target')).to.be.undefined;
            }
        });
    });
    it('should find external links and apply an external link icon', function() {
        var source = '<html><body><a href="http://internal.url/1" class="target">Link 1 (internal)</a>'
                + '<a href="http://external.url/2" class="target">Link 2 (external)</a>'
                + '<a href="http://external.url/3" class="target">Link 3 (external)</a>'
                + '<a href="http://another.url/4" class="target">Link 4 (external)</a></body></html>',
            $ = cheerio.load(source),
            internalLocation = 'internal.url';
        LinkBandit.init('.target', internalLocation, 'link__custom-external', url.parse, $);
        $('.target').each(function(i, el) {
            if (i === 1 || i === 2 || i === 3) {
                expect($(el).hasClass('link__custom-external')).to.equal(true);
            }
            else {
                expect($(el).hasClass('link__custom-external')).to.equal(false);
            }
        });
    });
    it('should find external links and set an external link relation', function() {
        var source = '<html><body><a href="http://internal.url/1" class="target">Link 1 (internal)</a>'
                + '<a href="http://external.url/2" class="target">Link 2 (external)</a>'
                + '<a href="http://external.url/3" class="target">Link 3 (external)</a>'
                + '<a href="http://another.url/4" class="target">Link 4 (external)</a></body></html>',
            $ = cheerio.load(source),
            internalLocation = 'internal.url';
        LinkBandit.init('.target', internalLocation, 'link__custom-external', url.parse, $);
        $('.target').each(function(i, el) {
            if (i === 1 || i === 2 || i === 3) {
                expect($(el).attr('rel')).to.equal('external');
            }
            else {
                expect($(el).attr('rel')).to.be.undefined;
            }
        });
    });
});

describe('isExternalLink', function() {
    it('should flag non-matching hosts as external', function() {
        var internalURL = 'http://internal.url',
            externalURL = 'http://external.url',
            internalLocation = 'internal.url';
        expect(LinkBandit.isExternalLink(internalURL, internalLocation, url.parse)).to.equal(false);
        expect(LinkBandit.isExternalLink(externalURL, internalLocation, url.parse)).to.equal(true);
    });
    it('should support checking a link against multiple internal hosts', function() {
        var internalURL1 = 'http://internal.url',
            internalURL2 = 'http://another-internal.url',
            internalURL3 = 'http://also.this.one',
            externalURL = 'http://external.url',
            internalLocations = ['internal.url', 'another-internal.url', 'also.this.one'];
        expect(LinkBandit.isExternalLink(internalURL1, internalLocations, url.parse)).to.equal(false);
        expect(LinkBandit.isExternalLink(internalURL2, internalLocations, url.parse)).to.equal(false);
        expect(LinkBandit.isExternalLink(internalURL3, internalLocations, url.parse)).to.equal(false);
        expect(LinkBandit.isExternalLink(externalURL, internalLocations, url.parse)).to.equal(true);
    });
    it('should treat an undefined or empty host as internal', function() {
        var undefinedHost1 = '/relative/path',
            undefinedHost2 = '?query',
            internalLocation = 'internal.url';
        expect(LinkBandit.isExternalLink(undefinedHost1, internalLocation, url.parse)).to.equal(false);
        expect(LinkBandit.isExternalLink(undefinedHost2, internalLocation, url.parse)).to.equal(false);
    });
    it('should treat standalone anchor links as internal', function() {
        var anchor1 = '#',
            anchor2 = '#anchor',
            anchor3 = 'http://internal.url/with/path#anchor-with-host-and-path',
            anchor4 = 'http://internal.url/with/query?hello=world#anchor-with-path-and-query',
            anchor5 = 'http://external.url#external-anchor',
            internalLocation = 'internal.url';
        expect(LinkBandit.isExternalLink(anchor1, internalLocation, url.parse)).to.equal(false);
        expect(LinkBandit.isExternalLink(anchor2, internalLocation, url.parse)).to.equal(false);
        expect(LinkBandit.isExternalLink(anchor3, internalLocation, url.parse)).to.equal(false);
        expect(LinkBandit.isExternalLink(anchor4, internalLocation, url.parse)).to.equal(false);
        expect(LinkBandit.isExternalLink(anchor5, internalLocation, url.parse)).to.equal(true);
    });
    it('should support detecting subdomains', function() {
        var internalURL1 = 'http://internal.url',
            internalURL2 = 'http://subdomain.internal.url',
            externalURL = 'http://external.url',
            internalLocation = 'internal.url';
        expect(LinkBandit.isExternalLink(internalURL1, internalLocation, url.parse)).to.equal(false);
        expect(LinkBandit.isExternalLink(internalURL2, internalLocation, url.parse)).to.equal(false);
        expect(LinkBandit.isExternalLink(externalURL, internalLocation, url.parse)).to.equal(true);
    });
    // eg. If "internal.url" is an internal location, "subdomain.internal.url" is valid but "not-internal.url" is not
    it('should not treat a partial match of a domain as a subdomain', function() {
        var internalURL1 = 'http://internal.url',
            internalURL2 = 'http://subdomain.internal.url',
            externalURL1 = 'http://external.url',
            externalURL2 = 'http://not-internal.url',
            internalLocation = 'internal.url';
        expect(LinkBandit.isExternalLink(internalURL1, internalLocation, url.parse)).to.equal(false);
        expect(LinkBandit.isExternalLink(internalURL2, internalLocation, url.parse)).to.equal(false);
        expect(LinkBandit.isExternalLink(externalURL1, internalLocation, url.parse)).to.equal(true);
        expect(LinkBandit.isExternalLink(externalURL2, internalLocation, url.parse)).to.equal(true);
    });
});
