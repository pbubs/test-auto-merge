/*eslint no-console: 0, new-cap: 0*/
var topNavMobileHandlers = {
    classlist: {
        active:        'js-navbar__dropdown--active',
        mobile:        'navbar__dropdown--mobile',
        mobileHidden:  'navbar__dropdown--mobile-hidden',
        slideLeft:     'navbar__dropdown--slide-left',
        slideRight:    'navbar__dropdown--slide-right',
        toggle:        'navbar__toggle',
        toggleCloned:  'navbar__toggle-cloned',
        navbar:        'navbar',
        dropdown:      'navbar__dropdown',
        itemslvl2:     'navbar__list-sub--level2',
        arrow:         'navbar__item-cta',
        arrowExpanded: 'navbar__item-cta--expanded'
    },
    entities: {
        expandableArrow: '<button title="Expand/Collapse Menu" class="navbar__item-cta desktop-hidden"></button>'
    },
    init: function() {
        var _this = this;
        // Mobile button toggler
        $('.navbar').UberAccordion({
            buttonClass: _this.classlist.toggle
        });
        _this.addExpandArrows(); //Add expandable arrows
        _this.handleClicks(); //Click listeners
        _this.handleMobileSwitch();
        _this.enableKeyboardAccessibility();
    },
    addExpandArrows: function() {
        var _this = this;

        $('.' + _this.classlist.navbar).find('.navbar__item, .navbar__item-sub').each(function() {
            var $thisNavbarItem = $(this),
                $thisNavbarItemList = $thisNavbarItem.find('.navbar__list-sub'),
                $thisNavbarItemLbl = $thisNavbarItem.children('.navbar__item-lbl, .navbar__item-sub-lbl');

            if ($thisNavbarItemList.length) {
                $thisNavbarItemLbl.find('.' + _this.classlist.arrow).remove();
                $thisNavbarItemLbl
                    .addClass('lbl--expandable')
                    .append(_this.entities.expandableArrow);

                if ($thisNavbarItem.hasClass('navbar__item')) {
                    $thisNavbarItem.addClass('navbar__item--w-children');
                } else {
                    $thisNavbarItem.addClass('navbar__item-sub--w-children');
                }
            }
        });
    },
    handleClicks: function() {
        var _this = this,
            $toggle = $('.' + _this.classlist.toggle),
            $navbar = $('.' + _this.classlist.navbar),
            $dropDown = $('.' + _this.classlist.dropdown);
        //Expand or Collapse the sub levels
        $navbar.on('click', '.' + _this.classlist.arrow, function(e) {
            e.preventDefault();
            var $listItemParent = $(this).closest('.navbar__item, .navbar__item-sub'),
                $childPanel = $listItemParent.find('.navbar__list-sub').eq(0);

            $childPanel.slideToggle('fast');
            $(this).toggleClass(_this.classlist.arrowExpanded);
        });

        //Handle the Open/Close of the nav
        $toggle.not('.' + _this.classlist.toggleCloned).on('click', function() {
            //Mobile menu is expanded?
            if ($dropDown.hasClass(_this.classlist.active)) {
                //Make it slide left
                $dropDown
                    .removeClass(_this.classlist.slideRight)
                    .addClass(_this.classlist.slideLeft);
                //Hide it. Timeout required due to the CSS transition, otherwise it would get skipped
                setTimeout(function() {
                    $dropDown
                        .removeClass(_this.classlist.active)
                        .addClass(_this.classlist.mobileHidden);
                    //Remove the cloned toggle 'X' that closes the nav
                    $('.' + _this.classlist.toggleCloned).remove();
                }, 1000);
            } else {
                //Clone the navbar toggle into the expanded nav
                $('.navbar__list').eq(0).find('li').eq(0)
                    .append($toggle.clone().addClass(_this.classlist.toggleCloned));

                //Collapse all the sections
                $('.navbar__list-sub').slideUp(1);

                //show the current page
                _this.showCurrentPage();
                //Make the mobile menu displayable
                $dropDown
                    .removeClass(_this.classlist.mobileHidden)
                    .addClass(_this.classlist.active)
                    .removeClass(_this.classlist.slideLeft);

                //Slide right the mobile menu to be visible
                setTimeout(function(){
                    $dropDown.addClass(_this.classlist.slideRight);
                }, 100);
            }
        });

        //Trigger the click of the real hamburger when the cloned one is clicked
        $navbar.on('click', '.' + _this.classlist.toggleCloned, function() {
            $toggle.not('.' + _this.classlist.toggleCloned).click();
        });
    },
    handleMobileSwitch: function() {
        var minMq = '(min-width: 60em)',
            _this = this,
            $navbarDropdown = $('.' + _this.classlist.dropdown),
            $navbarToggle = $('.' + _this.classlist.toggle);

        window.matchMedia(minMq).addListener(function(mql) {

            if (mql.matches) {
                //Close the mobile nav if the window is being resized with the nav open
                if ($navbarToggle.hasClass('uber-accordion__button-active')) {
                    $navbarToggle.click();
                }
                //Remove the JS hook call that controls the nav mobile
                $navbarDropdown.removeClass(_this.classlist.mobile);
                //Reset the navbar position on desktop
                $navbarDropdown.addClass(_this.classlist.active);

                $('.' + _this.classlist.itemslvl2).slideDown();

                //Close all the expanded sublevels to don't show up in desktop
                $('.navbar__list-sub').not('.' + _this.classlist.itemslvl2).slideUp();
            } else {
                //Load the arrows once again because the navigation is refreshed when reaching mobile
                _this.addExpandArrows();

                //Add a JS hook class to control the nav on mobile
                $navbarDropdown.addClass(_this.classlist.mobile);

                //Remove the class that styles the navbar on desktop
                $navbarDropdown.removeClass(_this.classlist.active);

                setTimeout(function() {
                    $('.' + _this.classlist.toggleCloned).remove();
                }, 1000);
            }
        });

        if (window.matchMedia(minMq).matches) {
            $navbarDropdown.removeClass(_this.classlist.mobile);
        } else {
            $navbarDropdown
                .addClass(_this.classlist.mobile)
                .addClass(_this.classlist.mobileHidden);
        }
    },
    showCurrentPage: function() {
        var _this = this;

        $('.navbar__item--current-parent').each(function(){
            $(this).find('.' + _this.classlist.arrow).eq(0).click();
        });
    },
    enableKeyboardAccessibility: function() {
        $('.navbar__item a').on('focus', function(){
            $(this).trigger('mouseenter');
        });
    }

};

(function() {
    topNavMobileHandlers.init();
}(jQuery));
