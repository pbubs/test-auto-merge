var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.config');

new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  hot: true,
  historyApiFallback: true,
  quiet: false,
  proxy: {
      '/monash/**': {
        target: 'http://mon-dev.clients.squiz.net',
        changeOrigin: true
      },
      '/monash-college-search-panel.html': {
        target: 'http://localhost:3000',
        secure: true,
        changeOrigin: true
      },
      '/js/**': {
        target: 'http://localhost:3000',
        secure: true,
        changeOrigin: true
      },
      '/styles/**': {
        target: 'http://localhost:3000',
        secure: true,
        changeOrigin: true
      },
  }
}).listen(config.port, 'localhost', function (err, result) {
  if (err) {
    return console.log(err);
  }

  console.log('Listening at http://localhost:' + config.port + '/');
});