var path = require('path');
var webpack = require('webpack');
var WebpackShellPlugin = require('webpack-shell-plugin');
var port = '3031';

module.exports = {
    entry: './dist/index.rolled.js',
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.webpack.min.js',
        publicPath: '/static/'
    },
    plugins: [
        new WebpackShellPlugin(
            {
                onBuildStart: [
                ],
                onBuildEnd: []
            }
        ),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        })
    ],
    module: {
        loaders: [{
            test: /\.js$/,
            loaders: [],
            include: path.join(__dirname, 'src')
        }]
    },
    port: port
};
