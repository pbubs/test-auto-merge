function initBannerSearch() {
  // Initialise a search URL as an URL object. Using this we can change the Search URL at anytime using Matrix Keyword on the HTML, circumventing the JS
  const searchPage = new URL(
    document.querySelector(
      '.banner-search__icon .banner-search__destination'
    ).textContent
  );

  document
    .querySelector('.banner-search__icon')
    .addEventListener('click', () => {
      const keywords =
        document.querySelector('.banner-search__input').value || '!padrenull'; //Temporary for now for empty seaches, need to look into how to handle empty queries

      searchPage.searchParams.append('query', keywords);

      location.href = searchPage;

      console.log('searchPage', searchPage);
    });
}

export { initBannerSearch };
