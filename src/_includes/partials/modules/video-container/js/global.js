$(function() {
    // Wrap YouTube iframes and Vimeo iframes with video container to maintain aspect
    // ratio at all screen sizes.
    // This is handled in JS because it's too hard for content editors to add manually.
    // $('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').filter(function() {
    //     // Make sure video container does not already exist
    //     return (!$(this).parents('.video-container').length);
    // }).wrap('<div class="video-container"></div>');
});
