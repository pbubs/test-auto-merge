/* global YT */
(function ($) {
    if (document.querySelector('iframe[data-youkuid-related], iframe[data-youkuid]')) {
        const config = {
            settings: {
                youtube: 'iframe[src*="youtube.com"], a[href*="youtube.com"][data-fallback-href], !.box-featured>*[href*="youtube.com"][data-fallback-href]',
                vimeo: 'iframe[src*="vimeo.com"], a[href*="vimeo.com"][data-fallback-href], !.box-featured>*[href*="vimeo.com"][data-fallback-href]',
                youku: 'iframe[src*="youku.com"], embed[src*="youku.com"]',
                ytID: 'yt',
                viID: 'vi',
                yoID: 'yo',
                containerVideo: 'video-container'
            },
            //Add uniq ID to iframe
            addID: (source, attr) => {
                if (source.length > 0) {
                    Array.from(source).forEach((el, i) => {
                        let count = i + 1;
                        el.setAttribute('id', attr + '-' + count);
                    });
                }
            },
            //Create div container outside iframe
            createContainer: (source) => {
                $(document).ready(function() {
                    const _this = config;
                    $(source).filter(function() {
                        // Make sure video container does not already exist
                        return (!$(this).parents(`.${_this.settings.containerVideo}`).length);
                    }).wrap(`<div class="${_this.settings.containerVideo}"></div>`);
                });
            },
            //Remove video container with iframe tag
            removeContainer: (source) => {
                source.parentElement.removeChild(source);
            },
            //Hide player (iframe)
            hide: (element) => {
                if (element.length > 0) {
                    Array.from(element).forEach(el => {
                        el.style.display = 'none';
                    });
                }
            },
            //Show player (iframe)
            show: (element) => {
                element.style.display = '';
            },
            //Add ?enablejsapi=1 to the and of YouTube URL
            appendYTjsAPI: (element) => {
                if (element.length > 0) {
                    Array.from(element).forEach(el => {
                        let tempAtrr = el.getAttribute('src');
                        tempAtrr = `${tempAtrr}?enablejsapi=1`;
                        el.setAttribute('src', tempAtrr);
                    });
                }
            },
            //Check if Youku ID exists required add in matrix to youtube in iframe data-youkuid
            isYoukuID: (element) => {
                if (element.length > 0) {
                    Array.from(element).forEach(el => {
                        return el.getAttribute('data-youkuid');
                    });
                }
            },
            //check if exist the same data-youkuid and data-youkuid-related
            check: (element, data) => {
                let temp;
                Array.from(element).forEach(el => {
                    if(el.getAttribute(data) === temp){
                        let tempatrr = el.getAttribute(data);
                        el.setAttribute(data, tempatrr +'1' );
                    }
                    temp = el.getAttribute(data);
                });
            }
        };

        const yts = document.querySelectorAll(`${config.settings.youtube}`);
        const vis = document.querySelectorAll(`${config.settings.vimeo}`);
        const yos = document.querySelectorAll(`${config.settings.youku}`);
        const ytImage = new Image();
        const ytPlayers = [];

        //API YouTube
        function loadYouTubeAPI() {
            var tag = document.createElement('script');
            var firstScriptTag = document.getElementsByTagName('script')[0];
            tag.src = 'https://www.youtube.com/iframe_api';
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        };

        //Fallback to Youku, replaces video with Youku alternative
        function fallbackToYouku(els) {
            Array.from(els).forEach(el => { 
                var fallbackHref = el.getAttribute('data-fallback-href');
                if (fallbackHref && fallbackHref !== '')  { 
                    // <a> links that open popups, just replace the href
                    el.setAttribute('data-original-href', el.href);
                    el.href = fallbackHref;
                } else { 
                    // Video container template prints out all to start with, so remove the unnecessary ones
                    let tempRelatedYoukuID = el.getAttribute('data-youkuid-related');
                    let tempRelatedYouku = document.querySelector(`[data-youkuid='${tempRelatedYoukuID}']`);
                    if (el.getAttribute('data-youkuid-related') !== '') {
                        config.removeContainer(el);
                        config.createContainer(tempRelatedYouku);
                        config.show(tempRelatedYouku);
                    } else {
                        config.removeContainer(el);
                        console.log(`YouTube iframe id="${el.getAttribute('id')} Please add Youku id.`);
                    }                   
                }
            });
        }
        //Load the favicon and run the callback
        function checkIfBlocked(icoURL, blocked, notBlocked) {
            //Waiting until the DOMContentReady event has fired will improve performance when YouTube isn't available
            $(document).ready(function() {
                var icon = new Image();
                icon.addEventListener('load', notBlocked);
                icon.addEventListener('error', blocked);
                icon.src = icoURL;
            });
        }

        //Init YouTube Players
        function onYouTubeIframeAPIReady() {
            Array.from(yts).forEach(el => {
                ytPlayers.push(new YT.Player(el.getAttribute('id'), {
                    events: {
                        'onError': onPlayerError
                    }
                }));
            });
        };

        //YouTube Error event
        function onPlayerError(event) {
            var errorStatus = event.data,
                errorTarget = event.target;
            var thisPlayerYT = document.querySelectorAll(`#${errorTarget.a.id}`);
            if (thisPlayerYT !== null) {
                if (errorStatus === 2) {
                    fallbackToYouku(thisPlayerYT);
                    console.log(`YouTube iframe id="${errorTarget.a.id}". The request contains an invalid parameter value. For example, this error occurs if you specify a video ID that does not have 11 characters, or if the video ID contains invalid characters, such as exclamation points or asterisks. Error Status = ${errorStatus}`);
                } else if (errorStatus === 5) {
                    fallbackToYouku(thisPlayerYT);
                    console.log(`YouTube iframe id="${errorTarget.a.id}". The requested content cannot be played in an HTML5 player or another error related to the HTML5 player has occurred. Error Status = ${errorStatus}`);
                } else if (errorStatus === 100) {
                    fallbackToYouku(thisPlayerYT);
                    console.log(`YouTube iframe id="${errorTarget.a.id}". The video requested was not found. This error occurs when a video has been removed (for any reason) or has been marked as private. Error Status = ${errorStatus}`);
                } else if (errorStatus === 101) {
                    fallbackToYouku(thisPlayerYT);
                    console.log(`YouTube iframe id="${errorTarget.a.id}". The owner of the requested video does not allow it to be played in embedded players. Error Status = ${errorStatus}`);
                }
            }
        };

        //Add ID to YouTube, Vimeo, Youku links
        config.addID(yts, config.settings.ytID);
        config.addID(vis, config.settings.viID);
        config.addID(yos, config.settings.yoID);
        config.check(yts, 'data-youkuid-related');
        config.check(vis, 'data-youkuid-related');
        config.check(yos, 'data-youkuid');
        //Hide Youku player
        config.hide(yos);
        
        //If YouTube videos exist
        if (yts.length > 0) {
            var blocked = function() {
                fallbackToYouku(yts);
            }
            var notBlocked = function() {
                //Add api scripts to head
                loadYouTubeAPI();
                //Create container outside youtube iframe tag
                config.createContainer(yts);
                //Add YouTube jsapi to the end of scr link
                config.appendYTjsAPI(yts);
            }
            checkIfBlocked('https://youtube.com/favicon.ico', blocked, notBlocked);
        }
        //If Vimeo videos exist
        if (vis.length > 0) {
            var blocked = function() {
                fallbackToYouku(vis);
            }
            var notBlocked = function() {
                //Create container outside Vimeo iframe tag
                config.createContainer(vis);
            }
            checkIfBlocked('https://vimeo.com/favicon.ico', blocked, notBlocked);
        }
    }
}(jQuery)); 