function initMicrobannerSearch() {
  // Initialise a search URL as an URL object. Using this we can change the Search URL at anytime using Matrix Keyword on the HTML, circumventing the JS
  const searchPage = new URL(
    document.querySelector(
      '#microbanner-search-form .banner-search__destination'
    ).textContent
  );

  document
    .querySelector('#microbanner-search-form .microbanner-search-btn')
    .addEventListener('click', () => {
      const keywords =
        document.querySelector('#microbanner-search').value || '!padrenull'; //Temporary for now for empty seaches, need to look into how to handle empty queries

      searchPage.searchParams.append('query', keywords);

      location.href = searchPage;

      console.log('searchPage', searchPage);
    });
}

export { initMicrobannerSearch };
