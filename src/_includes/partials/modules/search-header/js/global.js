var $headerSearchWrapper = $('.header-row__search'),
    $headerSearchToggle = $headerSearchWrapper.find('.header-row__search-icon');

var headerSearch = {
    init: function() {
        var thisObj = this;
        thisObj.setListeners();
        thisObj.handleSubmission();
        thisObj.handleMobileDesktopSwitch(); //Switch the DOM position of the search for mobile/destop
    },
    setListeners: function() {
        var headerSearchObj = this;
        $headerSearchToggle.on('click', headerSearchObj.handleSearchFormExpand);
        $headerSearchWrapper
            .find('input')
            .bind('focus blur', headerSearchObj.disableAutoZoom); //Disable auto zoom in iOS
    },
    handleSearchFormExpand: function(e) {
        var $slidingPanel = $('.header-row__search-form'),
            $searchInput = $slidingPanel.find('#quick-search__query, #query');
        e.preventDefault();

        //Handle opening/closing the search form
        $slidingPanel.toggleClass('header-row__search-form--expanded');
        $headerSearchWrapper.toggleClass('header-row__search--active');

        //Clear input and focus
        setTimeout(function() {
            $searchInput.val('');
            $searchInput.focus();
        }, 500);

        // if ($slidingPanel.hasClass('header-row__search-form--expanded')) {
        //     $slidingPanel.slideDown('fast');
        // } else {
        //     $slidingPanel.slideUp('fast');
        // }
    },
    handleMobileDesktopSwitch: function() {
        var minMq = '(min-width: 60em)',
            $searchForm = $('.header-row__search-form');

        window.matchMedia(minMq).addListener(function(mql) {
            if (mql.matches) {
                $searchForm.appendTo($('.header-row__search'));
            } else {
                $searchForm.insertAfter($('.header-row__search').parent());
            }
        });

        if (!window.matchMedia(minMq).matches) {
            $searchForm.insertAfter($('.header-row__search').parent());
        }
    },
    disableAutoZoom: function(event) {
        var $viewportMeta = $('meta[name="viewport"]');
        $viewportMeta
            .attr('content', 'width=device-width,initial-scale=1,maximum-scale=' + (event.type === 'blur' ? 10 : 1));
    },
    handleSubmission: function() {
        //TBC
    }
};

(function() {
    if ($headerSearchWrapper.find('form').length !== 0) {
        headerSearch.init();
    }
}(jQuery));
