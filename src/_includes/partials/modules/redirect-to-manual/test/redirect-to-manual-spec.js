// redirect-to-manual-spec.js
/*eslint-env mocha*/
var expect = require('chai').expect;

describe('RedirectToManual', function() {
    it('should exist', function() {
        expect(require('../js/redirect-to-manual')).to.be.defined;
    });
    var RedirectToManual = require('../js/redirect-to-manual');

    // Checking pathname
    describe('#pathnameEndsInHelp', function() {
        it('should return True if pathname ends in _help', function(){
            var pathname = '/_help',
                actual = RedirectToManual.pathnameEndsInHelp(pathname),
                expected = true;
            expect(actual).to.equal(expected);
        });
        it('should return False if pathname ends in something else', function(){
            var pathname = '/monash-mango/_study/why-choose-monash/course-search-box',
                actual = RedirectToManual.pathnameEndsInHelp(pathname),
                expected = false;
            expect(actual).to.equal(expected);
        });
        it('should return True if any pathname ends in _help', function(){
            var pathname = '/monash-mango/_study/why-choose-monash/course-search-box/_help',
                actual = RedirectToManual.pathnameEndsInHelp(pathname),
                expected = true;
            expect(actual).to.equal(expected,'expected true for input ' + pathname);
        });
    });

    // redirect page with pathname ends in _help  to manual page

    describe ('#redirectIfHelpPath', function(){
        it('should redirect to manual path when the pathname ends in _help only', function(){
            var fakeWindow = {
                 location: 'not manual location'
            };
            var pathname = '/_help';
            var manualPath = 'http://mon-stage.clients.squiz.net/monash-mango/_manual';
            RedirectToManual.redirectIfHelpPath(fakeWindow, pathname);
            expect(fakeWindow.location).to.equal(manualPath);
        });
        it('should not redirect if the pathname does not end in _help', function(){
            var fakeWindow = {
                 location: 'not manual location'
            };
            var pathname = '/monash-mango/_study/why-choose-monash/course-search-box';
            RedirectToManual.redirectIfHelpPath(fakeWindow, pathname);
            expect(fakeWindow.location).to.equal('not manual location');
        });
    });
});




