var RedirectToManual = {
    /**
     * Pathname ends in Help.
     * Returns true if the path parameter ends in '/_help', and false otherwise.
     *
     * @param  {String} path The pathname to test.
     * @return {Boolean}     True if the pathname ends in help.
     */
    pathnameEndsInHelp: function(path){
        var re = /\/_help$/g;
        return re.test(path);
    },
    /**
     * Redirect If Help Path
     * Redirect the page to Manual page if the pathname ends in _help
     * @param  {Object} win  The global window object
     * @param  {String} path The pathname
     */
    redirectIfHelpPath: function(win, path){
        if (this.pathnameEndsInHelp(path)) {
            win.location = 'http://mon-stage.clients.squiz.net/monash-mango/_manual';
        }
    }
};

if ((typeof module !== 'undefined') && (typeof module.exports !== 'undefined')) {
    module.exports = RedirectToManual;
}

// execute the function on browsers only
if (typeof window !== 'undefined') {
    RedirectToManual.redirectIfHelpPath(window, location.pathname);
}
