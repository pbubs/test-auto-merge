function initSearchPage() {
  const curURL = new URL(location.href);
  console.log('current URL', curURL);
  const pbCatFields = document.querySelectorAll(
    '.search-listing__category .search-listing__catogery-field'
  );
  const pbSelectedCat = curURL.searchParams.getAll('meta_category_or');
  const searchLists = document.querySelectorAll(
    '.search-listing__filter-wrapper'
  );

  searchLists.forEach((list) => {
    list
      .querySelector('.search-listing__category--label')
      .addEventListener('click', () => {
        list
          .querySelector('.search-listing__category')
          .classList.toggle('hidden');
      });
  });

  document
    .querySelector('.search-listing__category--mobile-close')
    .addEventListener('click', () => {
      document
        .querySelector('#search-listing__category--mobile')
        .classList.toggle('hidden');
    });

  pbCatFields.forEach((cb) => {
    if (pbSelectedCat.indexOf(cb.value) > -1) {
      cb.checked = true;
    } else {
      cb.checked = false;
    }
    cb.addEventListener('change', () => {
      curURL.searchParams.delete('meta_category_or');
      pbCatFields.forEach((cbInner) => {
        if (cbInner.checked) {
          curURL.searchParams.append('meta_category_or', cbInner.value);
        }
      });

      console.log('updated URL', curURL);

      location.href = curURL;
    });
  });
}

export { initSearchPage };
