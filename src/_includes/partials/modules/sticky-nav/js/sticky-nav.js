const stickyNav = () => {
  // This only guarantees that the header will be sticky. If anything else is supposed to be sticky, it won't work.

  console.log('StickyNavJS');

  const header = document.querySelector('#header');

  // ensure all parents of the header have overflow visible. This is done to ensure the sticky works.
  let a = header;

  let parents = [];

  while (a) {
    parents.unshift(a.parentNode);
    a = a.parentElement;
  }
  parents = parents.slice(3);

  console.log(parents);

  // Ensure all parent elements have overflow visible, and make sure that all children of the parent (except header) has the parent's old overflow property.
  parents.forEach((parent) => {
    let parentOverflowProp = window.getComputedStyle(parent).overflow;

    if (parent.style.overflow !== 'visible') {
      parent.style.overflow = 'visible';

      parent.childNodes.forEach((child) => {
        if (
          // Ensure the child node is the correct type
          child !== header &&
          child !== document.querySelector('#main') &&
          child.tagName &&
          !['SCRIPT', 'STYLE'].includes(child.tagName)
        ) {
          child.style.overflow = parentOverflowProp;
        }
      });
    }
  });

  console.log('successfully changed parent overflows');

  let titleHidden = false;
  const stickyTitle = () => {
    // You can probably optimise this with intersection observer by only running window.onscroll when data-template="rtf-to-html" is <0.3 or something
    const title = $('#header');

    if (window.scrollY >= 190 && !titleHidden) {
      title.addClass('stickyTitleFadeOut');
      titleHidden = true;
    } else if (window.scrollY < 189 && titleHidden) {
      title.removeClass('stickyTitleFadeOut');
      titleHidden = false;
    }
  };
  // dynamically set the top height for the sticky (in case header changes height);

  const setCssTopSticky = () => {
    //console.log('calculate css top sticky');
    if ($(window).width() <= 960) {
      // set mobile sticky height
      $('#header').css('top', '0');
    } else {
      // set desktop sticky height
      $('#header').css(
        'top',
        $('#header').children().eq(1).height() - $('#header').height() + 'px'
      );
    }
  };

  // whenever window resizes, redo the sticky tops
  let wasMobileWidth = $(window).width() <= 960;
  let throttled = false;

  const resizeEvent = () => {
    if ($(window).width() <= 960 && !wasMobileWidth) {
      // desktop -> mobile
      //console.log('it is mobile, it is significant');
      setTimeout(setCssTopSticky, 100);
      wasMobileWidth = true;
      window.addEventListener('scroll', stickyTitle);
    } else if ($(window).width() > 960 && wasMobileWidth) {
      // mobile -> desktop
      setTimeout(setCssTopSticky, 100);
      //console.log('it is desktop, it is significant');
      wasMobileWidth = false;
      $('#header').removeClass('stickyTitleFadeOut');

      window.removeEventListener('scroll', stickyTitle);
    } else {
      if (!throttled) {
        //console.log('throttle because not significant');
        setCssTopSticky();
        throttled = true;
        setTimeout(() => (throttled = false), 1000);
      }
    }
  };

  $(window).resize(resizeEvent);

  setTimeout(setCssTopSticky, 1000);

  if (wasMobileWidth) window.addEventListener('scroll', stickyTitle);
};

export { stickyNav };
