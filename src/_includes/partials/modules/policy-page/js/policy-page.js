const policyPageJS = () => {
  if ($('[data-template="rtf-to-html"]').length !== 0) {
    main();
  }
};

const main = () => {
  //console.log('Run Policy/Procedure JS');
  // Designate the page as a policy/procedure page with css identifier
  $('#page-wrapper').attr('data-css-identifier', 'rtf-to-html');

  const formatTables = () => {
    $('#page-wrapper td').each(function (index) {
      //const title = e.closest('table thead').length !== 0 ? e.closest('table thead').toArray() : parentTable.first('tr').toArray();

      const e = $(this);

      const test = 7;
      const trash = 2222;
      var meme = 3;
      meme = 999;

      const parentTable = e.closest('table');

      //console.log(e.closest('table'));

      // Add data attributes to every row

      if (parentTable.data('ignore') !== undefined) return; // have a special instance where a table can be made to ignore the responsive tables script if results are undesirable

      if (
        parentTable.find('th').length === 0 &&
        parentTable.find('thead').length === 0
      ) {
        if (
          parentTable
            .children(':first')
            .children(':first')
            .find('strong').length === 1
        ) {
          // it is a row table
          //console.log('row', e);
          if (!parentTable.hasClass('responsiveRow'))
            parentTable.addClass('responsiveRow');
        } else {
          // It is a 'data table' used only for layout
          //console.log('data table', parentTable);
        }
      } else if (
        parentTable.children(':first').children(':first').find('th').length ===
        1
      ) {
        // it is a row table
        //console.log('row', e);
        if (!parentTable.hasClass('responsiveRow'))
          parentTable.addClass('responsiveRow');
      } else {
        // it is a col table
        /*console.log(
          'col',
          e,
          e[0].cellIndex,
          parentTable.children(':first').children(':first').find('th').length
        );
        */

        const title =
          parentTable.children(':first').children(':first').find('th').toArray()
            .length == 0
            ? parentTable
                .children(':first')
                .children(':first')
                .find('td')
                .toArray()
            : parentTable
                .children(':first')
                .children(':first')
                .find('th')
                .toArray();

        const selfIndex =
          e[0].cellIndex < title.length ? e[0].cellIndex : title.length - 1;

        e.attr('data-label', title[selfIndex].textContent.trim());

        if (!parentTable.hasClass('responsiveCol')) {
          parentTable.addClass('responsiveCol');
        }
      }
    });
  };

  formatTables();
  
  // Separate Part into different section, and subsection into their own divs so they can easily be hidden/shown.
  const everythingWrapper = $('<div></div>', {
    'data-template': 'rtf-to-html'
  });

  let sectionWrapper = $('<div></div>', { class: 'section' });
  let sectionIntro = $('<div></div>', { class: 'section__intro' });
  let subSectionWrapper = $('<div></div>', { class: 'section__subsection' });
  let isFirstSection = true;
  let hasSubSection = false;
  let hasIntro = false;

  // Adds section and section__subsection wrappers to everything
  $('[data-template="rtf-to-html"]')
    .children()
    .each(function () {
      if (isFirstSection) {
        // if it is a section heading and it is the first section then
        // Need a better way of detecting first section.
        sectionIntro = $('<div></div>', { class: 'section__intro' }).append(
          $(this)
        );
        isFirstSection = false;
      } else if ($(this).data('type') === 'section-heading') {
        // if it is section heading, and it is not the first section, append and create a new section wrapper.
        if (hasSubSection) {
          sectionWrapper.append(subSectionWrapper);
        } else {
          sectionWrapper.append(sectionIntro);
        }
        everythingWrapper.append(sectionWrapper);
        sectionWrapper = $('<div></div>', { class: 'section' });
        sectionIntro = $('<div></div>', { class: 'section__intro' }).append(
          $(this)
        );
        hasSubSection = false;
        hasIntro = false;
      } else if (
        $(this).data('type') === 'section__subsection-heading' &&
        hasSubSection
      ) {
        // if it is a subsection, and it is not the first one add it and create new
        sectionWrapper.append(subSectionWrapper);
        subSectionWrapper = $('<div></div>', {
          class: 'section__subsection'
        }).append($(this));
        hasSubSection = true;
        hasIntro = true;
      } else if ($(this).data('type') === 'section__subsection-heading') {
        // if it is a subsection, and it is the first one, create new
        sectionWrapper.append(sectionIntro);
        subSectionWrapper = $('<div></div>', {
          class: 'section__subsection'
        }).append($(this));
        hasSubSection = true;
        hasIntro = true;
      } else if (!hasIntro) {
        // if it is not special and section has no intro, it is part of intro
        sectionIntro.append($(this));
      } else {
        // if it not special, and not part of intro, it is in subsection
        subSectionWrapper.append($(this));
      }
    });
  // append the last section with subsection or sectionIntro as needed
  hasSubSection
    ? sectionWrapper.append(subSectionWrapper)
    : sectionWrapper.append(sectionIntro);

  everythingWrapper.append(sectionWrapper);

  $('[data-template="rtf-to-html"]').replaceWith(everythingWrapper);

  // Construct Contents LHS Menu
  const lhsNav = $('<div></div>', { class: 'lhs-nav', id: 'lhs-nav' });
  const lhsNavList = $('<ul></ul>', {
    class: 'lhs-nav-list',
    'aria-expanded': 'true'
  });
  const contentHeading = $('<li></li>', {
    class: 'lhs-nav-list__item--parent'
  });
  const anchorLinks = $('<ul></ul>', {
    class: 'lhs-nav-list mobile-hidden',
    'aria-expanded': 'true',
    'data-type': 'content-menu'
  });

  let subSectionNumber = 1;

  const smoothScroll = (event) => {
    event.preventDefault();
    if (event.target.hash !== '') {
      //console.log(event);

      // Prevent default anchor click behavior

      const skipLength =
        $('#header').outerHeight() +
        $('.content-inner__main>h1').outerHeight() +
        ($(window).width() <= 960 ? 20 : 40);

      // Store hash
      const hash = event.target.hash;

      const scrollTo = $(hash).offset().top - skipLength;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate(
        {
          scrollTop: scrollTo
        },
        800,
        function () {
          // Add hash (#) to URL when done scrolling (default click behavior)

          history.pushState({}, '', event.target.href);
          //window.location.hash = hash;
        }
      );

      return false;
    } // End if
  };

  $('[data-type="section-heading"]').each(function (index) {
    $(this).attr('id', 'section-' + index);

    // Split the heading if it has a - or . in it.
    const [preHeading, heading] = $(this)
      .text()
      .trim()
      .split(/[.]*[\–\-\.:]{1}\s/);

    const sectionButton = $('<a></a>', {
      class: 'js-site-section-nav-link lhs-nav-list__item-link',
      title:
        'Expand the content menu and this section on screen and hide other subsections or sections',
      href: '#' + 'section-' + index
    }).on('click', smoothScroll);

    // If heading does not have 0 or . then just add text, otherwise wrap and stylise it.
    heading
      ? sectionButton
          .text(heading.trim())
          .prepend($('<span></span>').text(preHeading.trim() + ': '))
      : sectionButton.text(preHeading.trim());

    sectionButton.attr('name', '#' + $(this).attr('id'));

    const showHideSubContentButton = $('<button></button>', {
      class: 'lhs-nav-list__sub-content-button',
      for: 'Expand/Collapse-' + index
    }).text('+');

    showHideSubContentButton.click((e) => {
      const subnav = $(e.target).parent().siblings('ul');

      if (subnav.attr('aria-expanded') == 'false') {
        subnav.css({
          display: 'block'
        });
        subnav.attr('aria-expanded', 'true');
        subnav.addClass('lhs-nav-list__sub-content-button-open');
        $(e.target).text('-');
      } else {
        subnav.css({
          display: 'none'
        });
        subnav.attr('aria-expanded', 'false');
        subnav.removeClass('lhs-nav-list__sub-content-button-open');
        $(e.target).text('+');
      }
    });

    const sectionContentWrapper = $('<div></div>', {
      class: 'lhs-nav-list__item--lvl3--wrapper'
    });

    const subSectionsContent = $('<ul></ul>', {
      class: 'lhs-nav-list',
      'aria-expanded': 'false' /*,
      style: 'display:none;'*/
    });

    const sectionContent = $('<li></li>', {
      class:
        'js-site-section-nav-item lhs-nav-list__item lhs-nav-list__item--lvl3'
    }).append(sectionContentWrapper);

    let hasSubSections = false;

    $(this)
      .parent()
      .parent()
      .find('[data-type="section__subsection-heading"]')
      .each(function () {
        $(this).attr('id', 'subsection-' + subSectionNumber);

        const subSectionsItem = $('<li></li>', {
          class: 'lhs-nav-list__item lhs-nav-list__item--lvl4'
        });

        hasSubSections = true;
        /* Do not trim numbers
        const subSectionHeadingTextSplit = $(this)
          .text()
          .trim();
          .split(/[0-9]+\./);*/

        const contentMenuSubSectionHeading =$(this)
        .text()
        .trim();

        const subSectionsLink = $('<a></a>', {
          class:
            'contentLink js-site-section-nav-link lhs-nav-list__item-link lhs-nav-list__item-link--lvl4',
          title:
            'Show this subsection on screen and hide all other subsections',
          href: '#' + 'subsection-' + subSectionNumber
        })
          .text(contentMenuSubSectionHeading)
          .on('click', smoothScroll);

        const subSectionsNumPrepend = $('<div></div>', {
          class: 'lhs-nav-list__item-number'
        }).text(
          subSectionNumber < 10 ? '0' + subSectionNumber : subSectionNumber
        );

        subSectionsContent.append(
          subSectionsItem
            .append(subSectionsNumPrepend)
            .append(subSectionsLink.attr('name', '#' + $(this).attr('id')))
        );
        subSectionNumber++;
      });

    if (hasSubSections) {
      sectionContentWrapper
        .append(sectionButton)
        .append(showHideSubContentButton);
      sectionContent.append(subSectionsContent);
    } else {
      sectionContentWrapper.append(sectionButton);
    }

    anchorLinks.append(sectionContent);
  });

  // breadcrumb js
  Array.from(document.getElementsByClassName('breadcrumbs__divider')).forEach(
    (e) => (e.innerText = '>')
  );

  const downloadPageAsPDF = () => {
    //console.log('print button clicked');
    const startTime = performance.now();
    const windowLocation = window.location.href;
    const documentPrintUrl =
      windowLocation.indexOf('?') > 0
        ? encodeURI(windowLocation + '&print=true')
        : encodeURI(windowLocation + '?print=true');
    $.ajax({
      url:
        'https://www.monash.edu/policyBankPoc/poc-of-policy-content-container/convertapihtmltopdf?url=' +
        documentPrintUrl,
      beforeSend: function (xhr) {
        xhr.overrideMimeType('text/plain; charset=x-user-defined');
      }
    }).done(function (data) {
      const downloadLink = JSON.parse(data).Files[0].Url;
      window.open(downloadLink);
      console.log(
        'It took ' +
          performance.now() -
          startTime +
          ' milliseconds for the print button to show'
      );
    });
  };

  const printJsButton = $('<button></button>', {
    class: 'lhs__print-pdf',
    role: 'button'
  })
    .text('Download PDF')
    .click(downloadPageAsPDF);

  // Add a button that controls the content menu on mobile
  const contentMenuToggle = () => {
    const contentMenu = $('[data-type="content-menu"]');
    const ariaAttribute = contentMenu.css('display') != 'none';
    contentMenu.attr('aria-expanded', ariaAttribute.toString());
    const navListHeading = $('.lhs-nav-list__item-link--heading');
    const lhs = $('.lhs');
    const lhsNavList = $('.lhs-nav-list');
    const lhsPdfButton = $('.lhs__print-pdf');
    const lhsPrevPdfButton = $('.selectric-lhs__prev-pdf');
    //console.log('clicked');
    const contentMenuButtonHamburger = $('.contentMenu_toggle.open');
    const contentMenuClose = $('.contentMenu_toggle-close');

    const addOpenMobileContentStyling = () => {
      navListHeading.addClass('lhs-modal');
      lhsPrevPdfButton.addClass('lhs-modal');
      lhs.addClass('lhs-modal');
      lhsNavList.addClass('lhs-modal');
      lhsPdfButton.addClass('lhs-modal');
    };

    const removeOpenMobileContentStyling = () => {
      navListHeading.removeClass('lhs-modal');
      lhsPrevPdfButton.removeClass('lhs-modal');
      lhs.removeClass('lhs-modal');
      lhsNavList.removeClass('lhs-modal');
      lhsPdfButton.removeClass('lhs-modal');
    };

    if (contentMenu.attr('aria-expanded') == 'true') {
      contentMenu.attr('aria-expanded', 'false');
      contentMenu.addClass('mobile-hidden');
      //onsole.log('true, so hide');
      contentMenuClose.hide();
      contentMenuButtonHamburger.show();
      removeOpenMobileContentStyling();
    } else {
      contentMenu.attr('aria-expanded', 'true');
      contentMenu.removeClass('mobile-hidden');
      contentMenuClose.show();
      contentMenuButtonHamburger.hide();
      addOpenMobileContentStyling();
    }
  };

  const contentMenuButtonHamburger = $('<button></button>', {
    class: 'contentMenu_toggle open',
    'aria-controls': 'contentMenu_dropdown',
    role: 'button'
  })
    .text('Open Content Menu')
    .click(contentMenuToggle);

  const contentMenuClose = $('<button></button>', {
    class: 'contentMenu_toggle-close',
    'aria-controls': 'contentMenu_dropdown',
    role: 'button',
    id: 'contentMenu_toggle-close'
  })
    .click(contentMenuToggle)
    .append(
      $('<img/>', {
        src:
          'https://www.monash.edu/__data/assets/file/0007/2179618/navigate-cross.svg'
      })
    );
  /* 
      $('<svg></svg>', {
        role: 'image'
      }).append(
        $('<use></use>', {
          href: 'https://www.monash.edu/__data/assets/file/0007/2179618/navigate-cross.svg'
        })
      ) */

  const contentMenuCloseLabel = $('<label></label>', {
    class: 'lhs__content-menu-close-label',
    for: 'contentMenu_toggle-close'
  }).text('Close Content Menu');
  /*
<svg>
          <use href="/assets/images/icon-spritesheet.svg#arrow-outline-down"></use>
        </svg>
    */

  const navListHeading = $('<div></div>', {
    class: 'lhs-nav-list__item-link--heading'
  })
    .append(contentMenuButtonHamburger)
    .append($('<span></span>').text('CONTENT'))
    .append(contentMenuCloseLabel)
    .append(contentMenuClose);

  // Button for printing previous versions
  const prevVersionsSelect = $('#lhs__prev-pdf').clone(false);
  const prevVersionsSelectric = $('.selectric-wrapper.selectric-lhs__prev-pdf');

  prevVersionsSelect.on('change', function () {
    if (this.value) {
      //window.open(this.value, '_blank');
      //console.log($(this).find('option:selected').text());
      const href = this.value;
      const name = $(this).find('option:selected').text();
      //creating an invisible element
      var element = document.createElement('a');
      element.setAttribute('href', href);
      element.setAttribute('download', name + '.pdf');

      // Above code is equivalent to
      // <a href="path of file" download="file name">

      document.body.appendChild(element);

      //onClick property
      element.click();

      document.body.removeChild(element);
    }
  });

  const prevVersionsSelectLabel = $('<label></label>', {
    class: 'lhs__pref-pdf-label',
    for: 'lhs__prev-pdf'
  }).text('Previous Versions (PDF)');

  // Append everything to new-lhs and replace existing lhs
  const newLhs = $('<div></div>', { class: 'lhs' }).append(
    lhsNav.append(
      lhsNavList
        .append(navListHeading)
        .append(contentHeading)
        .append(anchorLinks)
        .append(printJsButton)
        .append(prevVersionsSelectLabel)
        .append(prevVersionsSelect ? prevVersionsSelect.clone(true) : '')
    )
  );
  prevVersionsSelectric.remove();

  const prepareContentMenu = () => {
    if ($(window).width() <= 960 && !wasMobileWidth) {
      //console.log('is mobile time to change');
    } else if (wasMobileWidth && $(window).width() > 960) {
      //console.log('is desktop time to change');
      $('.lhs').replaceWith(newLhs.clone(true));
      loadPrevVersionsSelectric();
    }
  };

  // manually set the 'top' css style for the sticky elements
  let throttled = false;
  let wasMobileWidth = $(window).width() <= 960;

  const setCssTopSticky = () => {
    //console.log('calculate css top sticky');
    if ($(window).width() <= 960) {
      // set mobile sticky height
      $('.microbanner').css(
        'top',
        $('#header .content-wrapper.flex-wrapper').height() -
          $('.microbanner').height() +
          20 +
          'px'
      );
      $('.lhs').css(
        'top',
        $('#header .content-wrapper.flex-wrapper').height() + 20 + 'px'
      );
    } else {
      // set desktop sticky height
      $('.microbanner').css(
        'top',
        $('#header').height() -
          $('.microbanner').height() -
          $('#header').children().eq(0).height() +
          40 +
          'px'
      );
      $('.content-inner__main > h1').css(
        'top',
        $('#header').height() -
          $('#header').children().eq(0).height() +
          40 +
          'px'
      );
      $('.lhs').css(
        'top',
        $('#header').height() -
          $('#header').children().eq(0).height() +
          40 +
          'px'
      );
    }
  };

  // whenever window resizes, redo the sticky tops
  $(window).resize(() => {
    prepareContentMenu();
    if ($(window).width() <= 960 && !wasMobileWidth) {
      // desktop -> mobile
      //console.log('it is mobile, it is significant');
      setTimeout(setCssTopSticky, 100);
      wasMobileWidth = true;
      setMobileSelectric();
    } else if ($(window).width() > 960 && wasMobileWidth) {
      // mobile -> desktop
      setTimeout(setCssTopSticky, 100);
      //console.log('it is desktop, it is significant');
      wasMobileWidth = false;
      setDesktopSelectric();
    } else {
      if (!throttled) {
        //console.log('throttle because not significant');
        setCssTopSticky();
        throttled = true;
        setTimeout(() => (throttled = false), 1000);
      }
    }
  });

  const setMobileSelectric = () => {
    // selectric needs to be reset on mobile with 0 styling
    $('.lhs .selectric-items').css({
      top: 'initial',
      left: 'initial',
      height: 'initial'
    });
  };

  const setDesktopSelectric = () => {
    // Open to the side
    const numItems = $('.lhs .selectric-items ul > li').length;
    //console.log('SELECTRIC ITEMS HIEGHT', $('.lhs .selectric-items').height())

    const selectricItemHeight = $('.lhs .selectric-items').height();

    $('.lhs .selectric-items').css({
      top: 'calc(100% - ' + (selectricItemHeight + 2) + 'px)',
      left: '100%',
      height: 'calc(' + numItems * 100 + '% +' + numItems + 'px)'
    });
  };

  // After 2 seconds of page initial load, set the sticky tops using js
  //setTimeout(setCssTopSticky, 2000);

  //let anchorThrottled = false;
  // Function controlling anchor link landing
  // The function actually applying the offset

  function offsetAnchor() {
    //console.log('offsetting');
    const skipLength =
      $('#header').outerHeight() +
      $('.content-inner__main>h1').outerHeight() +
      ($(window).width() <= 960 ? 20 : 40);
    const scrollTo = window.scrollY - skipLength;
    //console.log('scrollTo', scrollTo);
    if (location.hash.length !== 0) {
      window.scrollTo(0, scrollTo);
      //console.log('windowY', window.scrollY);
      setTimeout(() => {
        if (window.scrollY != scrollTo) {
          window.scrollTo(0, scrollTo);
        }
      }, 200);
    }
  }

  // Will trigger selectric() function only if it's lib included
  const loadPrevVersionsSelectric = () => {
    if ($.isFunction($.fn.selectric)) {
      $('#lhs__prev-pdf').selectric();
      //console.log('selectric Loaded');
      if ($(window).width() > 960) {
        setDesktopSelectric();
      }
      return;
    } else {
      window.setTimeout(loadPrevVersionsSelectric, 1000);
      //console.log('not loaded');
    }
  };

  const addClearFix = () => {
    if ($('.content.bg-white > .clearfix').length == 0) {
      $('.content.bg-white').append($('<div></div>', { class: 'clearfix' }));
    }
  };

  let microMHidden = false;
  const setMicrobannerStickyM = () => {
    // You can probably optimise this with intersection observer by only running window.onscroll when data-template="rtf-to-html" is <0.3 or something
    const microbannerM = $(
      '.microbanner .monash-m__wrapper.monash-m-microbanner > .monash-m-new.bg-white.semi-transparent'
    );

    if (window.scrollY > 195 && !microMHidden) {
      microbannerM.addClass('fadeOut');
      microMHidden = true;
    } else if (window.scrollY < 195 && microMHidden) {
      microbannerM.removeClass('fadeOut');
      microMHidden = false;
    }
  };

  // Reads url query strings
  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  $.when($('.lhs').replaceWith(newLhs.clone(true))).done(function () {
    addClearFix();

    if (!getParameterByName('print')) {
      // If there is no print query string
      window.setTimeout(offsetAnchor, 500);
      loadPrevVersionsSelectric();
      window.setTimeout(setCssTopSticky, 2000);
      window.onscroll = setMicrobannerStickyM;
      // Refresh svg elements because of JQuery being unable to insert SVG properly
      $('.contentMenu_toggle-close').html(
        $('.contentMenu_toggle-close').html()
      );
    }
  });
};

export { policyPageJS };
