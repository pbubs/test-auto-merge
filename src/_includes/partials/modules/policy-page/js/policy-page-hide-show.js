const policyPageJS = () =>
  !$('[data-template="rtf-to-html"]') ? console.log('no rtf-to-html') : main();

const main = () => {
  // Separate Part into different section, and subsection into their own divs so they can easily be hidden/shown.
  const everythingWrapper = $('<div></div>', {
    'data-template': 'rtf-to-html'
  });

  
  let sectionWrapper = $('<div></div>', { class: 'section' });
  let sectionIntro = $('<div></div>', { class: 'section__intro' });
  let subSectionWrapper = $('<div></div>', { class: 'section__subsection' });
  let isFirstSection = true;
  let hasSubSection = false;
  let hasIntro = false;

  // Adds section and section__subsection wrappers to everything
  $('[data-template="rtf-to-html"]')
    .children()
    .each(function () {
      if (isFirstSection) {
        // if it is a section heading and it is the first section then
        // Need a better way of detecting first section.
        sectionIntro = $('<div></div>', { class: 'section__intro' }).append(
          $(this)
        );
        isFirstSection = false;
      } else if ($(this).data('type') === 'section-heading') {
        // if it is section heading, and it is not the first section, append and create a new section wrapper.
        if (hasSubSection) {
          sectionWrapper.append(subSectionWrapper);
        } else {
          sectionWrapper.append(sectionIntro);
        }
        everythingWrapper.append(sectionWrapper);
        sectionWrapper = $('<div></div>', { class: 'section' });
        sectionIntro = $('<div></div>', { class: 'section__intro' }).append(
          $(this)
        );
        hasSubSection = false;
        hasIntro = false;
      } else if (
        $(this).data('type') === 'section__subsection-heading' &&
        hasSubSection
      ) {
        // if it is a subsection, and it is not the first one add it and create new
        sectionWrapper.append(subSectionWrapper);
        subSectionWrapper = $('<div></div>', {
          class: 'section__subsection'
        }).append($(this));
        hasSubSection = true;
        hasIntro = true;
      } else if ($(this).data('type') === 'section__subsection-heading') {
        // if it is a subsection, and it is the first one, create new
        sectionWrapper.append(sectionIntro);
        subSectionWrapper = $('<div></div>', {
          class: 'section__subsection'
        }).append($(this));
        hasSubSection = true;
        hasIntro = true;
      } else if (!hasIntro) {
        // if it is not special and section has no intro, it is part of intro
        sectionIntro.append($(this));
      } else {
        // if it not special, and not part of intro, it is in subsection
        subSectionWrapper.append($(this));
      }
    });
  // append the last section with subsection or sectionIntro as needed
  hasSubSection
    ? sectionWrapper.append(subSectionWrapper)
    : sectionWrapper.append(sectionIntro);

  everythingWrapper.append(sectionWrapper);

  $('[data-template="rtf-to-html"]').replaceWith(everythingWrapper);

  // show only first section when loading
  const showContentMenu = (e) => {
    e.parent()
      .children('button')
      .addClass('lhs-nav-list__item-cta--expanded')
      .data('expanded', 'true');
    e.parent().find('ul').show().attr('aria-expanded', 'true');
  };
  const hideContentMenu = (e) => {
    e.parent()
      .children('button')
      .removeClass('lhs-nav-list__item-cta--expanded')
      .data('expanded', 'false');
    e.parent().find('ul').hide().attr('aria-expanded', 'false');
  };
  const hideShowContentMenu = (e) =>
    e.data('expanded') === 'true' ? hideContentMenu(e) : showContentMenu(e);

  const hideEverything = () => {
    $('[data-type="section-heading"]').parents().hide();
    $('[data-type="section__subsection-heading"]').parents().hide();
  };

  const showThing = (e) => {
    e.parents().show();
    e.show();
    if (e.data('type') === 'section-heading') {
      e.parent().siblings().show();
    }
    return e;
  };

  const hideShowDivs = (e) => {
    switch (e) {
      case $(e.attr('name')).is(subsectionInView) &&
        e.hasClass('lhs-nav-list__item-link--lvl4'):
        showContentMenu(e);
      case e.hasClass('lhs-nav-list__item-link--lvl4'):
        hideOldShowNew(e);
      case $(e.attr('name')).is(subsectionInView):
        showContentMenu(e.parent());
      default:
        hideOldShowNew(e, true);
    } 
  };

  const hideOldShowNew = (e, needsParent = false) => {
    hideEverything();
    subsectionInView = $(e.attr('name'));

    if (needsParent) {
      if (subsectionInView.data('type') === 'section-heading') {
        showContentMenu(e.parent());
      } // If it is a section-heading, open the content menu to show subsections
      showThing(subsectionInView);
      return;
    }
    if (subsectionInView.data('type') === 'section-heading') {
      showContentMenu(e);
    } // If it is a section-heading, open the content menu to show subsections
    showThing(subsectionInView);
  };

  let subsectionInView = showThing($('[data-type="section-heading"]:first'));

  // Add id to every subsection heading and section
  $('[data-type="section__subsection-heading"]').each((index, element) =>
    $(element).attr('id', 'subsection-' + index)
  );
  $('[data-type="section-heading"]').each((index, element) =>
    $(element).attr('id', 'section-' + index)
  );

  // Construct Contents LHS Menu
  let lhsNav = $('<div></div>', { class: 'lhs-nav', id: 'lhs-nav' });
  let lhsNavList = $('<ul></ul>', {
    class: 'lhs-nav-list',
    'aria-expanded': 'true'
  });
  let contentHeading = $('<li></li>', {
    class: 'lhs-nav-list__item--parent'
  });
  let anchorLinks = $('<ul></ul>', {
    class: 'lhs-nav-list mobile-hidden',
    'aria-expanded': 'true'
  });

  let subSectionNumber = 0;
  $('[data-type="section-heading"]').each(function (index) {
    //
    let sectionButton = $('<button></button>', {
      class: 'js-site-section-nav-link lhs-nav-list__item-link',
      title:
        'Expand the content menu and this section on screen and hide other subsections or sections'
    }).text($(this).text());

    sectionButton.attr('name', '#' + $(this).attr('id'));

    // Replace buttonIcon with a text '+' when possible. No point having 2 buttons doing the same thing? Ask SMC.
    let buttonIcon = $('<button></button>', {
      class: 'lhs-nav-list__item-cta',
      type: 'button',
      title: 'Expand/Collapse',
      id: 'Expand/Collapse-' + index
    });

    let buttonIconLabel = $('<label></label>', {
      class: 'clip-out',
      for: 'Expand/Collapse-' + index
    }).text('Expand/Collapse  to show subsection headings');

    // This makes the tier 1 buttons act as a button to hide/show for sections
    sectionButton.click((e) => hideShowDivs($(e.currentTarget)));

    // This makes the icon open and close the content menu
    buttonIcon.click((e) => hideShowContentMenu($(e.currentTarget)));

    let sectionContentWrapper = $('<div></div>', {
      class: 'lhs-nav-list__item--lvl3--wrapper'
    }).append(sectionButton);

    let sectionContent = $('<li></li>', {
      class:
        'js-site-section-nav-item lhs-nav-list__item lhs-nav-list__item--lvl3'
    }).append(sectionContentWrapper);

    let subSectionsContent = $('<ul></ul>', {
      class: 'lhs-nav-list',
      'aria-expanded': 'false',
      style: 'display:none;'
    });
    let hasSubSections = false;

    $(this)
      .parent()
      .parent()
      .find('[data-type="section__subsection-heading"]')
      .each(function (subSec) {
        subSectionNumber++;
        let subSectionsItem = $('<li></li>', {
          class: 'lhs-nav-list__item lhs-nav-list__item--lvl4'
        });

        hasSubSections = true;

        let subSectionsLink = $('<button></button>', {
          class:
            'contentLink js-site-section-nav-link lhs-nav-list__item-link lhs-nav-list__item-link--lvl4',
          title: 'Show this subsection on screen and hide all other subsections'
        }).text(
          $(this)
            .text()
            .trim()
            .split(/[0-9]+\./)[1]
            .trim()
        );

        let subSectionsNumPrepend = $('<div></div>', {
          class: 'lhs-nav-list__item-number'
        }).text(
          subSectionNumber < 10 ? '0' + subSectionNumber : subSectionNumber
        );

        // toggle hide/show for subsections
        subSectionsLink.click((e) => hideShowDivs($(e.currentTarget)));

        subSectionsContent.append(
          subSectionsItem
            .append(subSectionsNumPrepend)
            .append(subSectionsLink.attr('name', '#' + $(this).attr('id')))
        );
      });

    if (hasSubSections) {
      sectionContent
        .append(buttonIconLabel)
        .append(buttonIcon)
        .append(subSectionsContent);
    }

    anchorLinks.append(sectionContent);
  });

  // Append everything to new-lhs and replace existing lhs
  $('.lhs-nav').replaceWith(
    $('<div></div>', { class: 'lhs' }).append(
      lhsNav.append(
        lhsNavList
          .append(
            $('<a></a>', { class: 'lhs-nav-list__item-link--parent' }).text(
              'CONTENT'
            )
          )
          .append(contentHeading)
          .append(anchorLinks)
      )
    )
  );
};
export { policyPageJS };
