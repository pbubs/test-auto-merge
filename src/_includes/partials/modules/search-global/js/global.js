var searchGlobal = {
    selector: '#page-wrapper',
    classInput: '.search-global__banner .tt-input',
    addClass: 'overlay',
    searchInput: $('#main-query'),
    dropdownButton: '.dropdown-toggle',

    init: function() {
        var self = this;

        this.autofocus();
        self.bind();
    },

    bind: function() {
        var self = this;

        // add class for overlay
        $(self.classInput).on('focus',function() {

            $(self.selector).addClass(self.addClass);

        });

        // delete class for overlay
        $(self.classInput).on('blur',function() {

            $(self.selector).removeClass(self.addClass);

        });

        // set width button dropdown from width dropdown menu
        // $(self.dropdownButton).each(function() {
        //     $(this).innerWidth($(this).next().innerWidth());
        // });

    },

    autofocus: function() {
        if(this.searchInput.val() == '') {
            this.searchInput[0].focus();
        }
    }

};

$(document).ready(function(){
    'use strict';

    searchGlobal.init();

});