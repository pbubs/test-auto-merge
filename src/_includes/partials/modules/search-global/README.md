# search-global

> Monash global search results page

## Overview

* OpsWiki: https://opswiki.squiz.net/Clients/MonashUniversity/Sites/Search
* Jira: https://jira.squiz.net/browse/MONFUNNEL-38
* Funnelback collection: https://mon3-funnelback01.squiz.net:8443/search/admin/index.cgi?collection=monash-main-search
* Cutup preview: [/source/html/search-global.html](https://monash.edu/__data/assets/git_bridge/0006/509343/deploy/search-global.html)