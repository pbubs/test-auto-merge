(function () {
    // Adds a class to a top wrapper that makes the header, footer and lHS nav to be hidden.
    // Used when the Timeline content page is loaded through an iframe in the popup.
    if (document.querySelector('.js-timeline-content-iframe-loaded')) {
        document.getElementById('page-wrapper').classList.add('timeline-content--iframed');
    }
}());
