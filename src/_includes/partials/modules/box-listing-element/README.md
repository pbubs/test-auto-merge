# Listing Box

See: [VMR-20: Listing Box](https://jira.squiz.net/browse/VMR-20).

Video behaviour is handled by the [squiz-module-magnific-popup](source/bower_components/squiz-module-magnific-popup) module.
