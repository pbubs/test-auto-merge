
    var renderTable;
    var clearDropdowns;

document.addEventListener('DOMContentLoaded', (event) => {
    const activityType = ['Demonstration', 'Lecture', 'Q&A panel', 'Tour', 'Webinar']
    const interestArea = ['Art, Design and Architecture', 'Arts, Humanities and Social Sciences', 'Business', 'Education', 'Engineering', 'Information Technology', 'Law', 'Medicine, Nursing and Health Sciences', 'Pharmacy and Pharmaceutical Sciences', 'Science', 'Campus life', 'General information' ]
    const dateType = ['Saturday 29 August', 'Sunday 30 August', 'Monday 31 August']

    const dateTypeMS = new MultiSelect('.date-multi-select', {
        items: dateType,
        display: 'value',
        current: null,
        parent: null,
        maxHeight: 0,
        sort: true,
        order: 'desc', // or arc
        more: '({X} selected)',
        placeholder: 'Date'
    });

    dateTypeMS.on('change', e => {
        obj = dateTypeMS.getCurrent()
        console.log(obj.length)
        for (let k in obj) {
        console.log(obj[k])

    }
    });

    const interestAreaMS = new MultiSelect('.interest-multi-select', {
        items: interestArea,
        display: 'value',
        current: null,
        parent: null,
        maxHeight: 0,
        sort: true,
        order: 'desc', // or arc
        more: '({X} selected)',
        placeholder: 'Interest area'
    });

    const activityTypeMS = new MultiSelect('.activity-multi-select', {
        items: activityType,
        display: 'value', // the property to use
        current: null,
        parent: null, // parent element
        maxHeight: 0,
        sort: true, // sort the dropdown list
        order: 'desc', // or arc
        more: '({X} selected)',
        placeholder: 'Activity type'
    });

    clearDropdowns = () => {
        Array.from(document.getElementsByClassName('si-selected')).forEach(e => e.click())
        document.getElementById('dateType').selectedIndex = 0;
        document.getElementById('activitySearch').value = '';
    }

    // Accessibility, making selects controllable by keyboard
    const openMultiSelect = (event) => {
        event.target.click();
    }

    $('#formInterestArea').on('keypress', openMultiSelect)
    $('#formActivityType').on('keypress', openMultiSelect)
    $('#dateType').on('keypress', openMultiSelect)

    // Add tab index to all multi select li
    $('.si-list > ul > li').attr('tabindex', 0).attr('role', 'option').attr('aria-selected', 'false').on('click', (e) => e.target.classList.contains("si-selected") ? e.target.setAttribute('aria-selected', 'true') : e.target.setAttribute('aria-selected', 'false') )


    renderModal = () => {
        var main = document.getElementById('page-overlay')
        var modal = document.getElementsByClassName("modal")[0];
        var modalContent = document.getElementsByClassName("modal")[0].childNodes[1].childNodes[3]; //modal-content
        var close = document.getElementsByClassName("close");

        function insertAfter(referenceNode, newNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
        }

        insertAfter(main, modal);

        for(k = 0; k < close.length;) {
        close[k].onclick = function () {
            modal.style.display = "none";
            modalContent.querySelectorAll('a').forEach(e => e.parentNode.removeChild(e));
        }
    }

        window.onclick = function(event) {
          if (event.target == modal) {
        modal.style.display = "none";
            modalContent.querySelectorAll('a').forEach(e => e.parentNode.removeChild(e));
          }
        }
    }

    renderTable = (filters) => {
        filters = filters || {};
        filters.interestArea = filters.interestArea || interestAreaMS.getCurrent().map(e => e.value);
        filters.activityType = filters.activityType || activityTypeMS.getCurrent().map(e => e.value);
        filters.dateType = filters.dateType || dateTypeMS.getCurrent().map(e => e.value);
        filters.search = filters.search || document.getElementById("activitySearch").value;

        var counter = 0
        document.getElementById('noneFound').style.display = "block";
        document.getElementById('activitiesTableEl').style.display = 'none';
        var code = activityData
            .filter(function(activity) {
                if(!activity.title) {
                    return false;
                }
                if(filters.dateType.length && !filters.dateType.some(e => {
                    var startDate = new Date(activity.start)
                    var startDateStr = startDate.toLocaleDateString("en-AU", {weekday: 'long', month: 'long', day: 'numeric' });
                    var rcomma = startDateStr.replace(/,/g, '');
                    return e === rcomma
                })
                ){
                    return false;
                }
                if(filters.interestArea.length && !filters.interestArea.some(e => e === activity.interestArea )) {
                    return false;
                }
                if(filters.activityType.length && !filters.activityType.some(e => e === activity.activityType.replace(/&amp;/g, '&'))) {
                    return false;
                }
                if(filters.search) {
                    var searchRE = new RegExp(filters.search, 'i');
        	        if(!searchRE.test(activity.title)) {
                        return false;
                    };
                }
                counter += 1
                document.getElementById('noneFound').style.display = "none";
                document.getElementById('activitiesTableEl').style.display = "block";

                return true;
            })
            .reduce(function(output, activity) {
                var calendar = "";
                var startDateStr = "";
                var startTimeStr= "";
                var endTimeStr = "";
                if(activity.start) {
                    var startDate = new Date(activity.start);
                    var endDate = new Date(activity.end);
                    startDateStr = startDate.toLocaleDateString("en-AU", {weekday: 'long', month: 'long', day: 'numeric' });
                    var outputFormatted = startDateStr.replace(/,/g, '');

                    // TODO: Fix abomination below
                    var sh = startDate.getHours(), sm = ('0' + startDate.getMinutes()).slice(-2);
                    var eh = endDate.getHours(), em = ('0' + endDate.getMinutes()).slice(-2);

                    if(sh && eh >= 12) {
        startTimeStr = startDate.toLocaleTimeString("en-AU", { hour: '2-digit', minute: '2-digit' }).replace(/^0(?:0:0?)?/, '').replace(" am", "am").replace(" pm", "pm").replace("pm", "").replace(":", ".");
                        endTimeStr = endDate.toLocaleTimeString("en-AU", {hour: '2-digit', minute: '2-digit'}).replace(/^0(?:0:0?)?/, '').replace(" pm", "pm").replace(":",".");
                    } else {
        startTimeStr = startDate.toLocaleTimeString("en-AU", { hour: '2-digit', minute: '2-digit' }).replace(/^0(?:0:0?)?/, '').replace(" am", "am").replace("am", "").replace(" pm", "pm").replace(":", ".");
                        endTimeStr = endDate.toLocaleTimeString("en-AU", {hour: '2-digit', minute: '2-digit'}).replace(/^0(?:0:0?)?/, '').replace(" am", "am").replace(":",".");
                    }

                    var calendar = createCalendar({
        options: {
        class: 'calendar-class'
                        },
                        data: {
        title: `${activity.title}`,
                          start: startDate,
                          duration: 120,
                          end: endDate,
                          address: '',
                          description: `${activity.description}`
                        }
                    })
                    calendar = calendar.innerHTML;
                }
                return output += `<tr>
        <td>${activity.title}</td>
        <td>${activity.interestArea}</td>
        <td>${activity.activityType}</td>
        <td>${outputFormatted}</td>
        <td>${startTimeStr} &ndash; ${endTimeStr}</td>
        <td>${calendar}</td>
    </tr>`;
            },"");

            var modal = document.getElementsByClassName("modal")[0];
            var modalContent = document.getElementsByClassName("modal")[0].childNodes[1].childNodes[3]; //modal-content
            var lblBtn = document.getElementsByClassName('add-to-calendar-checkbox');

            // TODO: No good.
            setTimeout(function() {
                for (i = 0; i < lblBtn.length;) {
        lblBtn[i].onclick = function (e) {
            modal.style.display = "block";
            var googleClone = e.target.nextSibling.cloneNode(true);
            var yahooClone = e.target.nextSibling.nextSibling.cloneNode(true);
            var iCalClone = e.target.nextSibling.nextSibling.nextSibling.cloneNode(true);
            var outlookClone = e.target.nextSibling.nextSibling.nextSibling.nextSibling.cloneNode(true);
            modalContent.appendChild(googleClone);
            modalContent.appendChild(yahooClone);
            modalContent.appendChild(iCalClone);
            modalContent.appendChild(outlookClone);
        }
    };
            }, 1000);

        document.getElementById("activitiesTableData").innerHTML = code;
    }

    renderTable();
    renderModal();
});
